# Tutoriels LP CISIIE

## module html avancé

Construire un tutoriel basé sur une démo sur un des thèmes listés ci-dessous.
Il faudra également prévoir une courte présentation (10mn/tuto) dans la semaine du 23-27 octobre.
Chaque tutoriel correspond à un des répertoires du projet.
Le tutoriel en lui-même, ou un lien, doit être décrit dans le fichier README.md du répertoire, en utilisant la syntaxe markdown.

## DATE DE RENDU : 4 novembre 2017, 24:00

## thèmes prévus :
1. css transition, multiple column layout: Renouard, Delamarre, Baier, Binet
2. css gradients, css color module : Marquant, Matmat, Jacquemin, Alhasne
3. filter effects, blending mode: Medjmadj, Elias, Elshobokshy, Cuny
4. css animation :Biselx, Escamilla, Penguilly, Demarbre
5. css transform : Grépin, Vincent, Parmentier, Dubois
6. grid, grid vs flex : Aubert, Lefevre, Sipp, Comte
7. postCss : Aubry, Wilmouth, Rosiak, André

## Ce qui est attendu :
1. introduction, rappels et pointeur vers la spécification W3C et une référence de qualité et complète
2. le tutoriel expliquant l'intéret et l'utilisation du module css, basé sur une démo
3. la démo elle-même, avec les sources
4. un point sur l'implantation dans les navigateurs
5. une liste de ressources/exemples/tutoriels existant sur le même thème


## Utilisation du dépôt

**Ne travaillez pas directement sur ce dépôt**

Faites un fork pour créer un dépôt dont vous êtes propriétaire.

Utilisez ce nouveau dépôt : clonez-le sur votre machine.

**Créez une branche portant le nom de votre tuto !**

Faites vos commits/push sur votre dépôt. Quand votre tutoriel est prêt, faites un pull request pour l'intégrer dans ce dépôt.
