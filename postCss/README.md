# Tutoriel LP CISIIE

## Module html avancé

## Thème du tutoriel :  PostCss

## Tutoriel :
1. Pré-requis
	* [NodeJS](https://nodejs.org/en/)
	* [NPM](https://nodejs.org/en/)
2. Installation
	```
	npm init
	npm install --save-dev gulp gulp-postcss gulp-sass
	```
3. Utilisation
	* Créer un fichier **gulpfile.js**
	* Importer gulp dans ce fichier  
	```js
	var gulp = require("gulp");
	var postcss = require("gulp-postcss");
	var sass = require("gulp-sass");
	```  
	* Créer une **tâche gulp**  
	```js
	gulp.task('css', function () {
	  var processors = [];
	  return gulp.src('./src/*.scss')
		.pipe(sass().on('error', sass.logError))
	    .pipe(postcss(processors))
	    .pipe(gulp.dest('./dest'));
	});
	```
	* Créer un répertoire **dest** et **src**
	* Écrire le code css dans le répertoire **src**
	* Dans le terminal taper la commande **gulp css**
4. Options :
	* Minifier  
	```
	npm install --save-dev cssnano
	```	
	* Importer cssnano  
	```js  
	var cssnano = require("cssnano");
	```
	* Modifier la tâche gulp précédente  
	```js
	gulp.task('css', function () {
	  var processors = [
		cssnano // Ajout du minifier
	  ];
	  return gulp.src('./src/*.scss')
		.pipe(sass().on('error', sass.logError))
	  	.pipe(postcss(processors))
		.pipe(gulp.dest('./dest'));
	});
	```
# Liens
* [PostCSS](http://postcss.org/)
* [PostCss Parts](https://www.postcss.parts/)
* [Grafikart - PostCSS](https://www.grafikart.fr/tutoriels/html-css/postcss-663)